# Web Frontend Mockup

# Job Requirements
Create a working static web frontend with 2 (3) pages that will be used to pitch an idea to a customer.  Future work on expanding the mockup and eventually on full implementation may be available. 


There will be "2" pages.

1. The first "resources" table list page built on data in the /data/ directory, and with screenshot in requirements/Test-screen-B1.pdf
Clicking on one of these lines should display the requirements/Test-screen-B2.pdf which is also connected to the /data/ and displays extra information not on B1 page.
It should be possible to input data, and this should be stored locally so returning to the page displays the new entered data.  However there is no need for persistance ibetween server reboots.

2. The Analytics page will display data, also from the /data/ directory.
A screen show is shown in requirements/Test-screen-B1.pdf
D3.js will be used to create the charts
Charts must use appropriate data from json files.







The web pages shall:

1. Be tested with Chrome only.
2. Be tested full screen only, resolution 2048 width, ie no need to have elements resize intelligently.


.
+-- Readme.md

+-- assets         -> Directory with icons

+-- data           -> Data to be used for charts and tables

+-- requirements   -> Two screenshots of what the frontend should look like






